
![Build status](https://gitlab.com/chevremw/lightpiio/badges/master/build.svg)


# LightPiIO

LightPiIO is a java library for the GPIO connector on *Pi. It intent to be small (only implementing the bus access, I2C, SPI, GPIO) and flexible. This implementation should work with every Pi-compatible devices as well as official RaspberryPi.

## Documentation

LightPiIO documentation is available [here](https://chevremw.gitlab.io/lightpiio).


## Why LightPiIO?

Some others awesome projects already exists for bus/gpio controll on RaspberryPi, often adapted for others platform (well known wiringPi, pi4j). But after passing hours trying to make them work on one microcomputer (OrangePi Zero+), after carefully reading the source code, it seems that many functions and path are hardcoded only for RPi platform, instead of using linux device files directly. This makes the porting of these libraries harder on other hardware.

For example: OrangePi Zero+ vs RaspberryPi

* /dev/spidev1.0 vs /dev/spidev0.0
* two i2c bus vs one
* ...

These incompatibilities prevent a quick and wide implementation on hybrid hardware. For every new hardware, a work has to be done on the base library to support it. (For example, pi4j embedd one version of wiringPi per __supported__ hardware!)

So, why the hell didn't they use linux files provided by users instead of hardcoding? Then, let's start from scratch and write one library that can communicate on any linux-recognized bus, without any guess on the physical system used.

## Building

Building is done with CMake. Assuming the sources are in src directory, create a build directory, then cd in.

```bash
$ ls
src
$ mkdir build
$ ls
src build
$ cd build
```

Then build the library:

```bash
$ cmake ../src
$ make
```

This will build the shared library used for JNI, then build the jar.

## Using LightPiIO

LightPiIO jar file can be used directly in your own project, using the classes.

Each bus-class also provide a main function, usefull for testing and debugging.

Exemple:
```bash
$ java -classpath LightPiIO-1.0.0.jar org.lightpiio.SPI /dev/spidev0.0 03 0F 00
```
Will perform a SPI communication on device represented by /dev/spidev0.0 with 3 bytes: 0x03 0x0F 0x00 (read register 0x0F on MCP2515), and output the reading as text.


## Work In Progress

This project is at an early stage and is still a work-in-progress. This is currently a pure-java library with some native functions for system call in C. As soon as possible, it will become a C/C++ library with java and python interfaces.
