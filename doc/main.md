# LightPiIO

## Introduction
 
LightPiIO is a multi-language library for embedded platform (such as RaspberryPi, OrangePi, BananaPi, TomatoPi, ...). It is light and not restricted to simo hardware, using directly device file instead of windows-like naming. As soon as you see the device file, LightPiIO can use it!

The source-code is hosted on gitlab at https://gitlab.com/chevremw/lightpiio
 
## Building LightPiIO

First, you need to install library depedencies, for package, and optionnaly to build documentation (using doxygen)

```bash
$ sudo apt-get install make cmake openjdk8 openjdk8-jre gcc g++
$ sudo apt-get install doxygen graphviz font-bitstream-type1 ghostscript-fonts
```

Then, clone git repository

```bash
git clone https://gitlab.com/chevremw/lightpiio.git
```

Then, create build directory and build

```bash
$ mkdir build && cd build
$ cmake ../lightpiio
$ make
$ make doc
```

### Cross-compiling support

If you wish to cross-compile (build for an other cpu architechture), you can specify following options to cMake:
```bash
$ cmake ../src -DCROSS_COMPILER_PREFIX=arm-linux-gnueabihf- -DCMAKE_TOOLCHAIN_FILE=../src/cMake/toolchain.cmake
```

This will build the library for ARM EABI5, using arm-linux-gnueabihf-gcc as c compiler.

## Languages

LightPiIO provide interfaces for:

* java

Soon available interfaces:

* C
* C++
* Python

