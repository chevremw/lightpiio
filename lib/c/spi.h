/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


#pragma once

#ifdef __cplusplus
extern "C" {
#endif
    
// INCLUDES
    
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
    
// CONSTANTS
    
#define LIGHTPIIO_SPI_RETURN_OK 0
#define LIGHTPIIO_SPI_ERROR -1
#define LIGHTPIIO_SPI_ERROR_IO -2

#define LIGHTPIIO_SPI_MODE_0 0
#define LIGHTPIIO_SPI_MODE_1 1
#define LIGHTPIIO_SPI_MODE_2 2
#define LIGHTPIIO_SPI_MODE_3 3

#define LIGHTPIIO_SPI_SPEED_1MHz 1000000
    
// FUNCTIONS
    
int lightpiio_spi_open(char* devname, uint64_t speed, uint64_t mode, int* fd);
int lightpiio_spi_close(int fd);
int lightpiio_spi_transfer(int fd, uint32_t len, uint8_t* txbuf, uint8_t* rxbuf);
    
#ifdef __cplusplus
}
#endif

