/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


#include "gpio.h"


int lightpiio_gpio_isExported(uint8_t pin, bool* isExported) {
    
    char dirname[] = "/sys/class/gpio/gpio0000" ;
    sprintf(dirname,"/sys/class/gpio/gpio%i" , pin) ;
    
    DIR* dir = opendir(dirname);
    if (dir) {
        *isExported = true;
        closedir(dir);
    } else if (ENOENT == errno) {
        *isExported = false;
    } else {
        return LIGHTPIIO_GPIO_ERROR;
    }
    
    return LIGHTPIIO_GPIO_RETURN_OK;
}

int lightpiio_gpio_export(uint8_t pin) {
    
    FILE * fd = fopen("/sys/class/gpio/export", "w");
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR;
    
    int r = fprintf(fd, "%i", pin);
    fclose(fd);
    
    if(r<0)
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    bool exp;
    r = lightpiio_gpio_isExported(pin, &exp);
    
    if(r != LIGHTPIIO_GPIO_RETURN_OK) return r;
    
    if(exp) return LIGHTPIIO_GPIO_RETURN_OK;
    else return LIGHTPIIO_GPIO_ERROR;
}

int lightpiio_gpio_unexport(uint8_t pin) {
    
    FILE * fd = fopen("/sys/class/gpio/unexport", "w");
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR;
    
    int r = fprintf(fd, "%i", pin);
    fclose(fd);
    
    if(r<0)
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    return LIGHTPIIO_GPIO_RETURN_OK;
}

int lightpiio_gpio_setDirection(uint8_t pin, uint8_t input_or_output) {
    
    if(input_or_output != LIGHTPIIO_GPIO_DIR_INPUT && input_or_output != LIGHTPIIO_GPIO_DIR_OUTPUT)
        return LIGHTPIIO_GPIO_ERROR_UNKNOWN_DIRECTION;
    
    bool exported;
    lightpiio_gpio_isExported(pin, &exported);
    
    if(!exported)
        return LIGHTPIIO_GPIO_ERROR_UNEXPORTED;
    
    char filename[] = "/sys/class/gpio/gpio0000/direction";
    sprintf(filename, "/sys/class/gpio/gpio%i/direction", pin);
    
    FILE *fd = fopen(filename, "w");
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR;
    
    int r = fprintf(fd, (input_or_output == LIGHTPIIO_GPIO_DIR_INPUT) ? "in" : "out");
    fclose(fd);
    
    if(r<0)
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    return LIGHTPIIO_GPIO_RETURN_OK;
}

int lightpiio_gpio_getDirection(uint8_t pin, uint8_t* input_or_output) {
    bool exported;
    lightpiio_gpio_isExported(pin, &exported);
    
    if(!exported)
        return LIGHTPIIO_GPIO_ERROR_UNEXPORTED;
    
    char filename[] = "/sys/class/gpio/gpio0000/direction";
    sprintf(filename, "/sys/class/gpio/gpio%i/direction", pin);
    
    FILE *fd = fopen(filename, "r");
    char dirout[] = "___";
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR;
    
    char* r = fgets(dirout, 4, fd);
    fclose(fd);
    
    if( r == NULL )
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    r = strstr(dirout, "in");
    if(r == dirout) {
        *input_or_output = LIGHTPIIO_GPIO_DIR_INPUT;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    r = strstr(dirout, "out");
    if(r == dirout) {
        *input_or_output = LIGHTPIIO_GPIO_DIR_OUTPUT;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    return LIGHTPIIO_GPIO_ERROR;    
}

int lightpiio_gpio_isInput(uint8_t pin, bool *isInput) {
    
    uint8_t inout;
    int r = lightpiio_gpio_getDirection(pin, &inout);
    
    *isInput = inout == LIGHTPIIO_GPIO_DIR_INPUT;
    
    return r;
}

int lightpiio_gpio_isOutput(uint8_t pin, bool *isOutput) {
    
    uint8_t inout;
    int r = lightpiio_gpio_getDirection(pin, &inout);
    
    *isOutput = inout == LIGHTPIIO_GPIO_DIR_OUTPUT;
    
    return r;
}

int lightpiio_gpio_setTrigger(uint8_t pin, uint8_t trigger_type) {
    
    if(trigger_type != LIGHTPIIO_GPIO_INT_NONE &&
        trigger_type != LIGHTPIIO_GPIO_INT_RISING &&
        trigger_type != LIGHTPIIO_GPIO_INT_FALLING &&
        trigger_type != LIGHTPIIO_GPIO_INT_BOTH)
        return LIGHTPIIO_GPIO_ERROR_UNKNOWN_DIRECTION;
    
    
    bool exported;
    lightpiio_gpio_isExported(pin, &exported);
    
    if(!exported)
        return LIGHTPIIO_GPIO_ERROR_UNEXPORTED;
    
    char filename[] = "/sys/class/gpio/gpio0000/edge";
    sprintf(filename, "/sys/class/gpio/gpio%i/edge", pin);
    
    FILE *fd = fopen(filename, "w");
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR;
    
    int r;
    
    if(trigger_type == LIGHTPIIO_GPIO_INT_NONE)
        r = fprintf(fd, "none");
    
    if(trigger_type == LIGHTPIIO_GPIO_INT_RISING)
        r = fprintf(fd, "rising");
    
    if(trigger_type == LIGHTPIIO_GPIO_INT_FALLING)
        r = fprintf(fd, "falling");
    
    if(trigger_type == LIGHTPIIO_GPIO_INT_BOTH)
        r = fprintf(fd, "both");
    
    fclose(fd);
    
    if(r<0)
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    return LIGHTPIIO_GPIO_RETURN_OK;
}

int lightpiio_gpio_getTrigger(uint8_t pin, uint8_t* trigger_type) {
    
    bool exported;
    lightpiio_gpio_isExported(pin, &exported);
    
    if(!exported)
        return LIGHTPIIO_GPIO_ERROR_UNEXPORTED;
    
    char filename[] = "/sys/class/gpio/gpio0000/edge";
    sprintf(filename, "/sys/class/gpio/gpio%i/edge", pin);
    
    FILE *fd = fopen(filename, "r");
    char edgeout[] = "________";
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR;
    
    char* r = fgets(edgeout, 8, fd);
    fclose(fd);
    
    if( r == NULL )
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    r = strstr(edgeout, "none");
    if(r == edgeout) {
        *trigger_type = LIGHTPIIO_GPIO_INT_NONE;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    r = strstr(edgeout, "rising");
    if(r == edgeout) {
        *trigger_type = LIGHTPIIO_GPIO_INT_RISING;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    r = strstr(edgeout, "falling");
    if(r == edgeout) {
        *trigger_type = LIGHTPIIO_GPIO_INT_FALLING;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    r = strstr(edgeout, "both");
    if(r == edgeout) {
        *trigger_type = LIGHTPIIO_GPIO_INT_BOTH;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    return LIGHTPIIO_GPIO_ERROR; 
}

int lightpiio_gpio_waitForInterrupt(uint8_t pin, uint64_t timeout) {
    
    bool exported;
    lightpiio_gpio_isExported(pin, &exported);
    
    if(!exported)
        return LIGHTPIIO_GPIO_ERROR_UNEXPORTED;
    
    struct pollfd polls ;
	char c;
	char inpath[] = "/sys/class/gpio/gpio0000/value";
    sprintf(inpath, "/sys/class/gpio/gpio%i/value", pin);
    
	polls.fd     = open(inpath, O_RDWR) ;	
	polls.events = POLLPRI ;	// Urgent data!

	// First flush
    while(read (polls.fd, &c, 1) > 0) ;
	
	// Wait for it ... 
	int x = poll (&polls, 1, timeout) ;

	// Read the status
    while(read (polls.fd, &c, 1) > 0) ;
	close(polls.fd);

	if(x < 0)
        return LIGHTPIIO_GPIO_ERROR;
    
    return LIGHTPIIO_GPIO_RETURN_OK;
}

int lightpiio_gpio_digitalWrite(uint8_t pin, bool value) {
    
    bool exported;
    lightpiio_gpio_isExported(pin, &exported);
    
    if(!exported)
        return LIGHTPIIO_GPIO_ERROR_UNEXPORTED;
    
    char filename[] = "/sys/class/gpio/gpio0000/value";
    sprintf(filename, "/sys/class/gpio/gpio%i/value", pin);
    
    FILE *fd = fopen(filename, "w");
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR;
    
    int r = fprintf(fd, (value) ? "1" : "0");
    fclose(fd);
    
    if(r<0)
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    return LIGHTPIIO_GPIO_RETURN_OK;
}

int lightpiio_gpio_digitalRead(uint8_t pin, bool* value) {
    
    bool exported;
    lightpiio_gpio_isExported(pin, &exported);
    
    if(!exported)
        return LIGHTPIIO_GPIO_ERROR_UNEXPORTED;
    
    char filename[] = "/sys/class/gpio/gpio0000/value";
    sprintf(filename, "/sys/class/gpio/gpio%i/value", pin);
    
    FILE *fd = fopen(filename, "r");
    char val[2];
    
    if(fd == NULL)
        return LIGHTPIIO_GPIO_ERROR_FILE;
    
    char* r = fgets(val, 2, fd);
    fclose(fd);
    
    if( r == NULL )
        return LIGHTPIIO_GPIO_ERROR_IO;
    
    if(*val == '0') {
        *value = false;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    if(*val == '1') {
        *value = true;
        return LIGHTPIIO_GPIO_RETURN_OK;
    }
    
    return LIGHTPIIO_GPIO_ERROR;
}
