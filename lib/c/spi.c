/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


#include "spi.h"

int lightpiio_spi_open(char* devname, uint64_t speed, uint64_t mode, int* fd) {
    
	*fd = open (devname, O_RDWR);
	
	if (*fd < 0)
		return LIGHTPIIO_SPI_ERROR;
	
	unsigned char spiBPW = 8;

	if (ioctl (*fd, SPI_IOC_WR_MODE, &mode) < 0)
		return LIGHTPIIO_SPI_ERROR_IO;

	if (ioctl (*fd, SPI_IOC_WR_BITS_PER_WORD, &spiBPW) < 0)
		return LIGHTPIIO_SPI_ERROR_IO;

	if (ioctl (*fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed) < 0)
		return LIGHTPIIO_SPI_ERROR_IO;
		
    return LIGHTPIIO_SPI_RETURN_OK;
}

int lightpiio_spi_close(int fd) {
    
    close(fd);
    return LIGHTPIIO_SPI_RETURN_OK;
}

int lightpiio_spi_transfer(int fd, uint32_t len, uint8_t* txbuf, uint8_t* rxbuf){
    
    struct spi_ioc_transfer spi ;
	spi.tx_buf        = (unsigned long)txbuf ;
	spi.rx_buf        = (unsigned long)rxbuf ;
	spi.len           = len ;
	spi.delay_usecs   = 0 ;
	spi.speed_hz      = 0 ;
	spi.bits_per_word = 0 ;
	spi.cs_change	  = 1 ;
	spi.pad			  = 0 ;
	spi.tx_nbits	  = 0 ;
	spi.rx_nbits	  = 0 ;
	
	if(ioctl (fd, SPI_IOC_MESSAGE(1), &spi) < 0)
        return LIGHTPIIO_SPI_ERROR_IO;
    
    return LIGHTPIIO_SPI_RETURN_OK;
}


