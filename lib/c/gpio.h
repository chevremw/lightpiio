/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// INCLUDES
    
#include <stdbool.h> 
#include <poll.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <sys/dir.h>
#include <errno.h>
#include <string.h>

// CONSTANTS
    
#define LIGHTPIIO_GPIO_RETURN_OK 0
#define LIGHTPIIO_GPIO_ERROR -1
#define LIGHTPIIO_GPIO_ERROR_IO -2
#define LIGHTPIIO_GPIO_ERROR_UNEXPORTED -3
#define LIGHTPIIO_GPIO_ERROR_UNKNOWN_DIRECTION -4
#define LIGHTPIIO_GPIO_ERROR_UNKNOWN_TRIGGER -5
#define LIGHTPIIO_GPIO_ERROR_FILE -6
    
#define LIGHTPIIO_GPIO_DIR_INPUT 1
#define LIGHTPIIO_GPIO_DIR_OUTPUT 0

#define LIGHTPIIO_GPIO_INT_NONE 0
#define LIGHTPIIO_GPIO_INT_RISING 1  
#define LIGHTPIIO_GPIO_INT_FALLING 2
#define LIGHTPIIO_GPIO_INT_BOTH 3
    
// FUNCTIONS
    
int lightpiio_gpio_isExported(uint8_t pin, bool* isExported);
int lightpiio_gpio_export(uint8_t pin);
int lightpiio_gpio_unexport(uint8_t pin);

int lightpiio_gpio_setDirection(uint8_t pin, uint8_t input_or_output);
int lightpiio_gpio_getDirection(uint8_t pin, uint8_t* input_or_output);
int lightpiio_gpio_isInput(uint8_t pin, bool *isInput);
int lightpiio_gpio_isOutput(uint8_t pin, bool *isOutput);

int lightpiio_gpio_setTrigger(uint8_t pin, uint8_t trigger_type);
int lightpiio_gpio_getTrigger(uint8_t pin, uint8_t* trigger_type);
int lightpiio_gpio_waitForInterrupt(uint8_t pin, uint64_t timeout);

int lightpiio_gpio_digitalWrite(uint8_t pin, bool value);
int lightpiio_gpio_digitalRead(uint8_t pin, bool* value);

    
#ifdef __cplusplus
}
#endif

