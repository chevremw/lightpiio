/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif
    
// INCLUDES
    
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
    
// CONSTANTS
    
#define LIGHTPIIO_I2C_RETURN_OK 0
#define LIGHTPIIO_I2C_ERROR -1
#define LIGHTPIIO_I2C_ERROR_IO -2
    
    
// FUNCTIONS
    
int lightpiio_i2c_open(char* devname, int* fd);
int lightpiio_i2c_close(int fd);

// THIS IS MASTER MODE
int lightpiio_i2c_requestFrom(int fd, uint16_t address, uint16_t flags, uint16_t len, uint8_t* buf);
int lightpiio_i2c_writeTo(int fd, uint16_t address, uint16_t flags, uint16_t len, uint8_t* buf);
    
    
#ifdef __cplusplus
}
#endif
