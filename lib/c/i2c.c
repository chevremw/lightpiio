/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


#include "i2c.h"


int lightpiio_i2c_open(char* devname, int* fd) {
    
	*fd = open (devname, O_RDWR);
    
    if(*fd < 0)
        return LIGHTPIIO_I2C_ERROR;
    
    return LIGHTPIIO_I2C_RETURN_OK;
}

int lightpiio_i2c_close(int fd) {
    
    close(fd);
    return LIGHTPIIO_I2C_RETURN_OK;
}

int lightpiio_i2c_requestFrom(int fd, uint16_t address, uint16_t flags, uint16_t len, uint8_t* buf)  {

    return lightpiio_i2c_writeTo(fd, address, flags | I2C_M_RD, len, buf);
}

int lightpiio_i2c_writeTo(int fd, uint16_t address, uint16_t flags, uint16_t len, uint8_t* buf) {
        
    struct i2c_msg msg;
	struct i2c_rdwr_ioctl_data data;
	
	data.msgs = &msg;
	data.nmsgs = 1;
	
	msg.addr = address;
	msg.flags = flags;
	msg.len = len;
	msg.buf = buf;
	
	if(ioctl (fd, I2C_RDWR, &data) < 0)
        return LIGHTPIIO_I2C_ERROR_IO;

    return LIGHTPIIO_I2C_RETURN_OK;
}
