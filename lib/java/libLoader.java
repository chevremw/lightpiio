/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


// From http://tlrobinson.net/blog/2009/03/embedding-and-loading-a-jni-library-from-a-jar/

package org.lightpiio;

import java.net.URL;
import java.util.zip.ZipFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

class libLoader {

    private libLoader() {
        try {
            // get the class object for this class, and get the location of it
            final Class c = libLoader.class;
            final URL location = c.getProtectionDomain().getCodeSource().getLocation();
            
            // jars are just zip files, get the input stream for the lib
            ZipFile zf = new ZipFile(location.getPath());
            InputStream in = zf.getInputStream(zf.getEntry("libLightPiIO.so"));
            
            // create a temp file and an input stream for it
            File f = File.createTempFile("JARLIB-", "-libLightPiIO.so");
            FileOutputStream out = new FileOutputStream(f);
            
            // copy the lib to the temp file
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0)
                out.write(buf, 0, len);
            in.close();
            out.close();
    
            // load the lib specified by it’s absolute path and delete it
            System.load(f.getAbsolutePath());
            f.delete();
            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    private static libLoader m_instance = null;
    
    public static libLoader getInstance() {
		if(m_instance == null) {
			m_instance = new libLoader();
		}
		return m_instance;
    }
}
