/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */
 
 
package org.lightpiio;

import java.io.*;


/**
 * This class handle functions for GPIO interface.
 */
public class GPIO
{
	private native static  int c_waitForInterrupt(String fl, int timeout) throws IOException;
	
	static {
		libLoader lb = libLoader.getInstance(); // Ensure lib is loaded
	}
	
	public static final boolean INPUT = true; /**< Convenient define for INPUT value */
	public static final boolean OUTPUT= false; /**< Convenient define for OUTPUT value */
	public static final String INT_NONE = "none"; /**< No interrupt */
	public static final String INT_RISING = "rising"; /**< Interrupt on rising edge */
	public static final String INT_FALLING= "falling"; /**< Interrupt on falling edge */
	public static final String INT_BOTH	= "both"; /**< Interrupt on rising and falling edge */
	
	/**
	 * Main function allow to use GPIO from command line.
	 */
	public static void main( String[] arg )
	{
		try {
			if(arg.length < 2) {
				printUsage();
				return;
			}
			
			final int pin = Integer.parseInt(arg[1]);
			
			switch(arg[0]) {
				case "export":
					export(pin);
					return;
				case "unexport":
					unexport(pin);
					return;
				case "read":
					System.out.println(digitalRead(pin));
					return;
				case "waitfor":
					System.out.println(waitForInterrupt(pin, -1));
					return;
				case "direction":
					if(arg.length > 2) { setDirection(pin, arg[2].equals("input")); return; }
					System.out.println(direction(pin));
					return;
				case "trigger":
					if(arg.length > 2) { setupInterrupt(pin, arg[2]); return; }
					System.out.println(trigger(pin));
					return;
				case "write":
					if(arg.length < 3) { printUsage(); return; } 
					digitalWrite(pin, arg[2].equals("0"));
					return;
			}
			
			if(arg[0].equals("event"))
			{
				pinCallback(pin, () -> {
					try {
						System.out.println(digitalRead(pin) ? "UP" : "DOWN");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}, -1);
			
				try {
					while (true) {
						Thread.sleep(500);
					}
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
				return;
			}

			printUsage();
		}
		catch(IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Print usage of main function on stdout
	 */
	protected static void printUsage() {
		System.out.println("Usage:");
		System.out.println("GPIO export #pin");
		System.out.println("GPIO unexport #pin");
		System.out.println("GPIO read #pin");
		System.out.println("GPIO write #pin [0|1]");
		System.out.println("GPIO direction #pin [in|out]");
		System.out.println("GPIO trigger #pin [none|rising|falling|both]");
		System.out.println("GPIO waitfor #pin");
	}
	
	/**
	 * Check if pin is already exported.
	 * @param pin Pin number.
	 */
	public static boolean isExported(int pin) {
		File f = new File("/sys/class/gpio/gpio"+pin);
		return(f.exists() && f.isDirectory());
	}
	
	private static void checkExported(int pin) throws IOException {
		if(!isExported(pin)) {
			throw new IOException("Pin is not exported!");
		}
	}
	
	/**
	 * Export pin if needed
	 * @param pin Pin number.
	 */
	public static void export(int pin) throws IOException {
		if(isExported(pin)) return;
		FileWriter writer = new FileWriter("/sys/class/gpio/export");
		writer.write(String.valueOf(pin));
		writer.close();
	}
	
	/**
	 * Unexport pin if exported
	 * @param pin Pin number.
	 */
	public static void unexport(int pin) throws IOException {
		if(!isExported(pin)) return;
		FileWriter writer = new FileWriter("/sys/class/gpio/unexport");
		writer.write(String.valueOf(pin));
		writer.close();
	}
	
	/**
	 * Set direction of pin
	 * @param pin Pin number. Must be already exported.
	 * @param isInput Set INPUT or OUTPUT
	 * @see INPUT
	 * @see OUTPUT
	 */
	public static void setDirection(int pin, boolean isInput) throws IOException {
		checkExported(pin);
		FileWriter writer = new FileWriter("/sys/class/gpio/gpio"+pin+"/direction");
		writer.write(isInput ? new String("in") : new String("out"));
		writer.close();
	}
	
	/**
	 * Read direction of the pin
	 * @param pin Pin number. Must be already exported.
	 * @return Pin direction as string.
	 */
	public static String direction(int pin) throws IOException {
		checkExported(pin);
		BufferedReader reader = new BufferedReader(new FileReader("/sys/class/gpio/gpio"+pin+"/direction"));
		return reader.readLine();
	}
	
	/**
	 * Read interrupt source for the pin
	 * @param pin Pin number. Must be already exported.
	 * @return Pin interrupt source as string.
	 * @see INT_NONE
	 * @see INT_BOTH
	 * @see INT_FALLING
	 * @see INT_RISING
	 */
	public static String trigger(int pin) throws IOException {
		checkExported(pin);
		BufferedReader reader = new BufferedReader(new FileReader("/sys/class/gpio/gpio"+pin+"/edge"));
		return reader.readLine();
	}
	
	/**
	 * Write value to pin
	 * @param pin Pin number. Must be already exported.
	 * @param val Value to write.
	 */
	public static void digitalWrite(int pin, boolean val) throws IOException {
		checkExported(pin);
		FileWriter writer = new FileWriter("/sys/class/gpio/gpio"+pin+"/value");
		writer.write(val ? new String("1") : new String("0"));	
		writer.close();
	}
	
	/**
	 * Read value of pin
	 * @param pin Pin number. Must be already exported.
	 * @return Pin state
	 */
	public static boolean digitalRead(int pin) throws IOException {
		checkExported(pin);
		FileReader reader = new FileReader("/sys/class/gpio/gpio"+pin+"/value");
		char[] cbuff = new char[1];
		reader.read(cbuff);
		reader.close();
		return cbuff[0] == '1';
	}
	
	/**
	 * Wait untill interrupt on pin occurs.
	 * @param pin Pin number. Must be already exported.
	 * @param timeout Timeout. A value of 0 is infinite wait.
	 * @return 0 if timeout, positive value if success, negative value if error (see poll system call)
	 */
	public static int waitForInterrupt(int pin, int timeout) throws IOException {
		checkExported(pin);
		return c_waitForInterrupt("/sys/class/gpio/gpio"+pin+"/value", timeout);
	}
	
	/**
	 * Interface for Interrupt run function
	 */
	public interface IntCallback {
        /**
         * Function run each time interrupt is issued.
         */
		public void execute();
	}
	
	/**
	 * Set up callback function for the pin. Callback function is then called each time interrupt is done. Caller can stop callback thread throw returned object.
	 * @param pin Pin number. Must be already exported.
	 * @param fnc Callback function.
	 * @param timeout Timeout. Value of zero means infinite wait.
	 * @return A thread running object.
	 */
	public static GPIORunCallback pinCallback(final int pin, final IntCallback fnc, final int timeout) throws IOException{
		checkExported(pin);

		GPIORunCallback bg = new GPIORunCallback() {
			
			class ProducerConsumer {
				int m_N = 0;
				public synchronized void acquire() {
					if(m_N == 0) {
						try {
							wait();
						} catch(InterruptedException e) {
							e.printStackTrace();
						}
					}
					m_N--;
				}
				public synchronized void release() {
					m_N++;
					notify();
				}
			};
			
			// Wait for event in background, execute callback at each trigger
			private ProducerConsumer m_number = new ProducerConsumer();
			
			public void run() {
				new Thread(new Runnable() {
					public void run() {
						while(true) {
							m_number.acquire(); // Wait to be unlocked
							fnc.execute();
						}
					}
				}).start(); // Execute callback in another thread to prevent blocking
			
				try {
					while(m_continue) {
						waitForInterrupt(pin, timeout);
						m_number.release(); // Unlock the running thread for one run
					}
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		
		// Start background thread
		Thread thread = new Thread(bg); // Waiting for event thread
		thread.start();
		return bg;
	}
	
	/**
	 * Setup interrupt triggers
	 * @param pin Pin number. Must be already exported.
	 * @param setup Trigger source.
	 * @see INT_NONE
	 * @see INT_BOTH
	 * @see INT_FALLING
	 * @see INT_RISING
	 */
	public static void setupInterrupt(int pin, String setup) throws IOException {
		checkExported(pin);
		switch(setup)
		{
			case INT_NONE:
			case INT_BOTH:
			case INT_FALLING:
			case INT_RISING:
				FileWriter writer = new FileWriter("/sys/class/gpio/gpio"+pin+"/edge");
				writer.write(setup);
				writer.close();
				break;
			default:
				throw new IOException("setup must be none, both, rising or falling");
		}
	}
}
