/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


#include <JNI_lightpiio.h>
#include <poll.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>


JNIEXPORT jint JNICALL Java_org_lightpiio_GPIO_c_1waitForInterrupt
  (JNIEnv *env, jclass cl, jstring fl, jint timeout)
{
	struct pollfd polls ;
	char c;
	const char*inpath = (*env)->GetStringUTFChars(env, fl, 0);
	
	polls.fd     = open(inpath, O_RDWR) ;
	
	(*env)->ReleaseStringUTFChars(env, fl, inpath);
	
	polls.events = POLLPRI ;	// Urgent data!

	// First flush
    while(read (polls.fd, &c, 1) > 0) ;
	
	// Wait for it ... 
	int x = poll (&polls, 1, timeout) ;

	// Read the status
    while(read (polls.fd, &c, 1) > 0) ;
	close(polls.fd);

	return x;
}

