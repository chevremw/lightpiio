/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


#include <JNI_lightpiio.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

JNIEXPORT jint JNICALL Java_org_lightpiio_I2C_openDevice
  (JNIEnv *env, jobject obj, jstring path)
{
	const char *inpath = (*env)->GetStringUTFChars(env, path, NULL);
	int fd ;
	
	fd = open (inpath, O_RDWR);
	(*env)->ReleaseStringUTFChars(env, path, inpath);

	jclass Exception = (*env)->FindClass(env, "java/io/IOException");
	if (fd < 0) return (*env)->ThrowNew(env, Exception,"Unable to open I2C device");
	
	return fd;
}


JNIEXPORT void JNICALL Java_org_lightpiio_I2C_close
  (JNIEnv *env, jobject obj, jint fd)
{
	close(fd);
}


JNIEXPORT jbyteArray JNICALL Java_org_lightpiio_I2C_requestFrom
  (JNIEnv *env, jobject obj, jint fd, jshort address, jshort flags, jshort len)
{
	struct i2c_msg msg;
	struct i2c_rdwr_ioctl_data data;
	
	data.msgs = &msg;
	data.nmsgs = 1;
	
	msg.addr = address;
	msg.flags = flags | I2C_M_RD;
	msg.len = len;
	
	jbyteArray ret = (*env)->NewByteArray(env, len);
	msg.buf = (*env)->GetByteArrayElements(env, ret, 0);
	
	int r =  ioctl (fd, I2C_RDWR, &data) ;
	(*env)->ReleaseByteArrayElements(env, ret, msg.buf, 0);
    
	jclass Exception = (*env)->FindClass(env, "java/io/IOException");
	if(r < 0) (*env)->ThrowNew(env, Exception,"Unable to perform request");
	
	return ret;
}


JNIEXPORT void JNICALL Java_org_lightpiio_I2C_writeTo
  (JNIEnv *env, jobject obj, jint fd, jshort address, jshort flags, jbyteArray dat)
{
	struct i2c_msg msg;
	struct i2c_rdwr_ioctl_data data;
	
	data.msgs = &msg;
	data.nmsgs = 1;
	
	msg.addr = address;
	msg.flags = flags;
	msg.len = (*env)->GetArrayLength(env, dat);
	msg.buf = (*env)->GetByteArrayElements(env, dat, 0);
	
	int r =  ioctl (fd, I2C_RDWR, &data) ;
	(*env)->ReleaseByteArrayElements(env, dat, msg.buf, 0);
    
	jclass Exception = (*env)->FindClass(env, "java/io/IOException");
	if(r < 0) (*env)->ThrowNew(env, Exception,"Unable to perform write");
	
}


JNIEXPORT void JNICALL Java_org_lightpiio_I2C_setupAdapter
  (JNIEnv *env, jobject obj, jint fd, jshort fn, jshort val)
{
	int r =  ioctl (fd, fn, &val) ;
    
	jclass Exception = (*env)->FindClass(env, "java/io/IOException");
	if(r < 0) (*env)->ThrowNew(env, Exception,"Unable to perform request");
}


JNIEXPORT jstring JNICALL Java_org_lightpiio_I2C_lastError
  (JNIEnv *env, jclass obj)
{
	char *msg = strerror (errno);
	jstring result = (*env)->NewStringUTF(env,msg); 
	return result;
}

