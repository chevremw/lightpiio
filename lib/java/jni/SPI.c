/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


#include <JNI_lightpiio.h>
#include <stdio.h>
#include <unistd.h>

#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

static int last_error;

// this has been refactored from WiringPi
JNIEXPORT jint JNICALL Java_org_lightpiio_SPI_openDevice
  (JNIEnv *env, jobject obj, jstring path, jint speed, jint spiMode)
{
	const char *inpath = (*env)->GetStringUTFChars(env, path, NULL);
	int fd ;
	
	jclass Exception = (*env)->FindClass(env, "java/io/IOException");
	fd = open (inpath, O_RDWR);
	
	(*env)->ReleaseStringUTFChars(env, path, inpath);
	
	if (fd < 0)
		return (*env)->ThrowNew(env, Exception,"Unable to open SPI device");
		//return wiringPiFailure (WPI_ALMOST, "Unable to open SPI device: %s\n", strerror (errno)) ;

	// Set SPI parameters.
	//	Why are we reading it afterwriting it? I've no idea, but for now I'm blindly
	//	copying example code I've seen online...
	
	unsigned char spiBPW = 8;

	if (ioctl (fd, SPI_IOC_WR_MODE, &spiMode)         < 0)
		return (*env)->ThrowNew(env, Exception,"SPI Mode Change failure");// wiringPiFailure (WPI_ALMOST, "SPI Mode Change failure: %s\n", strerror (errno)) ;

	if (ioctl (fd, SPI_IOC_WR_BITS_PER_WORD, &spiBPW) < 0)
		return (*env)->ThrowNew(env, Exception,"SPI BPW Change failure");//wiringPiFailure (WPI_ALMOST, "SPI BPW Change failure: %s\n", strerror (errno)) ;

	if (ioctl (fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed)   < 0)
		return (*env)->ThrowNew(env, Exception,"SPI Speed Change failure");//wiringPiFailure (WPI_ALMOST, "SPI Speed Change failure: %s\n", strerror (errno)) ;

	return fd ;
}

JNIEXPORT void JNICALL Java_org_lightpiio_SPI_close
  (JNIEnv *env, jobject obj, jint fid)
  {
	  close(fid);
  }
  
JNIEXPORT jbyteArray JNICALL Java_org_lightpiio_SPI_transfer
  (JNIEnv * env, jobject obj, jint fid, jint speed, jbyteArray payload)
{
	jsize len = (*env)->GetArrayLength(env, payload);
	jbyte* elm = (*env)->GetByteArrayElements(env, payload, 0);
	
	jbyteArray ret = (*env)->NewByteArray(env, len);
	jbyte* rete = (*env)->GetByteArrayElements(env, ret, 0);
	
	//printf("C: [%i] %i, %i, %i\n", len, elm[0], elm[1], elm[2]);
	
	int r = -1;
	struct spi_ioc_transfer spi ;
	spi.tx_buf        = (unsigned long)elm ;
	spi.rx_buf        = (unsigned long)rete ;
	spi.len           = len ;
	spi.delay_usecs   = 0 ;
	spi.speed_hz      = speed ;
	spi.bits_per_word = 8 ;
	spi.cs_change	  = 1 ;
	spi.pad			  = 0 ;
	spi.tx_nbits	  = 0 ;
	spi.rx_nbits	  = 0 ;
	
	//printf("C: [%i] %i, %i, %i\n", len, elm[0], elm[1], elm[2]);
	
	r =  ioctl (fid, SPI_IOC_MESSAGE(1), &spi) ;
    
	(*env)->ReleaseByteArrayElements(env, payload, elm, 0);
	(*env)->ReleaseByteArrayElements(env, ret, rete, 0);
	
	jclass Exception = (*env)->FindClass(env, "java/io/IOException");
	if(r < 0) (*env)->ThrowNew(env, Exception,"Unable to perform transfer");
	
	return ret;
}

JNIEXPORT jstring JNICALL Java_org_lightpiio_SPI_lastError
  (JNIEnv *env, jobject obj)
{
	char *msg = strerror (errno);
	jstring result = (*env)->NewStringUTF(env,msg); 
	return result;
}

