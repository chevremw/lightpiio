/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


package org.lightpiio;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class represent one SPI bus connection.
 * Only master mode is currently supported.
 */
public class SPI
{
	private native int openDevice(String device, int speed, int mode) throws IOException;
	private native void close(int fid);
	private native byte[] transfer(int fid, int speed, byte[] buf) throws IOException;
	private static native String lastError();

	public static final int MODE_0 = 0; /**< enum value MODE_0 (clock polarity=0, sample on rising edge)*/
	public static final int MODE_1 = 1; /**< enum value MODE_1 (clock polarity=0, sample on falling edge)*/
	public static final int MODE_2 = 2; /**< enum value MODE_2 (clock polarity=1, sample on falling edge)*/
	public static final int MODE_3 = 3; /**< enum value MODE_3 (clock polarity=1, sample on rising edge)*/
	public static final int DEFAULT_MODE = MODE_0;
	public static final int DEFAULT_SPEED = 1000000;

	private int m_fid = -1; /**< File descriptor */
	private int m_speed = DEFAULT_SPEED; /**< communication speed (bps) */
	private int m_mode = DEFAULT_MODE; /**< communication mode */
	
	private libLoader lb = libLoader.getInstance(); // Ensure lib is loaded
	private static HashMap<String, SPI> m_devices = new HashMap<String, SPI>(); /**< Hold already created instances. One instance per bus. */

	/**
	 * This is one main function that allow to communicate directly throw SPI bus. It takes bytes in hex format for transmission from master to slave. Transmission from slave to master is written to stdout byte per byte, hex formatted.
	 * Usage: SPI bus_device [byte 0] [byte 1] ... [byte n]
	 * Response: [byte 0] [byte 1] ... [byte n]
	 */	 
	public static void main( String[] arg )
	{
		String devname = null;
		Integer speed = DEFAULT_SPEED;
		Integer mode = DEFAULT_MODE;
	
		try
		{
			if(arg.length < 2 || arg[0] == "-h" || arg[0] == "--help") {
				printUsage();
				return;
			}
			
			devname = arg[0];
			SPI sample = getInstance(devname, speed, mode);

			byte[] tr = new byte[arg.length-1];
			
			for(int i=1;i<arg.length;i++) {
				tr[i-1] = (byte)Integer.parseInt(arg[i],16);
			}
			
			byte[] trb = sample.transfer(tr);
			
			for(int i=0;i<tr.length;i++) {
				System.out.print(String.format("%02x ", trb[i]));
			}
			System.out.println("");
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage()+": "+ lastError());
		}
	}
	
	/**
	 * This function print to stdout usage for main function.
	 */
	protected static void printUsage()
	{
		System.out.println("SPI filename data0 [data1 [ data2 ...] ]");
	}
	
	/**
	 * Constructor. Open and configure new SPI bus.
	 * @param dev Bus device filename
	 * @param speed Bus speed in bps
	 * @param mode Bus mode (0,1,2,3)
	 * @see MODE_0
	 * @see MODE_1
	 * @see MODE_2
	 * @see MODE_3
	 */
	private SPI(String dev, int speed, int mode) throws IOException {
		open(dev, speed, mode);
	}
	
	/**
	 * This function contains a destructor for SPI bus. Close the bus and clean.
	 */
	protected void finalize()
	{
		close();
	}
   
   /**
    * Close the bus connection.
    */
   private void close()
   {
		if(m_fid > 0) {
			close(m_fid);
			m_fid = -1;
		}
   }
   
   /**
    * This function give access to one SPI bus. It create and open it if needed.
     * @param devname Bus device filename
	 * @param speed Bus speed in bps
	 * @param mode Bus mode (0,1,2,3)
	 * @return A handler to SPI bus
	 * @see MODE_0
	 * @see MODE_1
	 * @see MODE_2
	 * @see MODE_3
	 */
   public static SPI getInstance(String devname, int speed, int mode) throws IOException {
		if(!m_devices.containsKey(devname)) {
			m_devices.put(devname, new SPI(devname, speed, mode));
		}
		return m_devices.get(devname);
	}
   
   /**
    * This function perform a SPI transfer.
    * @param buf Array of bytes to transfer from master to slave.
    * @return Array of bytes transferred from slave to master.
    */
   public synchronized byte[] transfer(byte[] buf) throws IOException
   {
		if(m_fid < 0) {
			throw new IOException("Device is not open! fid="+m_fid);
		}
		return transfer(m_fid, m_speed, buf);
   }
   
   /**
    * Perform a single byte transfer
    * @param buf Byte to transfer from master to slave.
    * @return Byte transferred from slave to master.
    */
   public synchronized byte transfer(byte buf) throws IOException
   {
		byte[] b = new byte[1];
		b[0] = buf;
		b = transfer(b);
		return b[0];
   }
   
   /**
	 * This function open and configure new SPI bus.
	 * @param dev Bus device filename
	 * @param speed Bus speed in bps
	 * @param mode Bus mode (0,1,2,3)
	 * @see MODE_0
	 * @see MODE_1
	 * @see MODE_2
	 * @see MODE_3
	 */
   private void open(String device, int speed, int mode) throws IOException
   {
		m_fid = openDevice(device, speed, mode);
		m_speed = speed;
		m_mode = mode;
   }
}

