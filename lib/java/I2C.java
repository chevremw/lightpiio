/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */


package org.lightpiio;

import java.io.*;
import java.util.HashMap;

/**
 * This class handle connection to I2C bus
 */
public class I2C {
	private native int openDevice(String devname) throws IOException;
	private native void close(int fid);
	private native byte[] requestFrom(int fid, short address, short flags, short length) throws IOException;
	private native void writeTo(int fid, short address, short flags, byte[] data) throws IOException;
	private native void setupAdapter(int fid, short fnc, short value) throws IOException;
	private static native String lastError();
	
	private static final short I2C_RETRIES    = 0x0701;
	private static final short I2C_TIMEOUT    = 0x0702;
	private static final short I2C_SLAVE      = 0x0703;
	private static final short I2C_SLAVE_FORCE= 0x0706;
	private static final short I2C_FUNCS      = 0x0705;
	private static final short I2C_RDWR       = 0x0707;
	
	private int m_fd=-1;
	
	private libLoader lb = libLoader.getInstance();
	
	private static HashMap<String, I2C> m_buses = new HashMap<String, I2C>();
	
	/**
	 * Main function allow to perform actions on commandline.
	 * Usage: I2C [bus_device] read [N]
	 * Usage: I2C [bus_device] write [byte 0] [byte 1] ... [byte N]
	 * Bytes are HEX-formatted.
	 */
	public static void main( String[] arg )
	{
		String devname = null;
	
		try
		{
			if(arg.length < 4 || arg[0] == "-h" || arg[0] == "--help") {
				printUsage();
				return;
			}
			
			devname = arg[0];
			
			I2C sample = getInstance(devname);
			
			byte devaddr = (byte)Integer.parseInt(arg[2],16);
			
			if(arg[1].equals("read"))
			{
				byte[] buf = sample.requestFrom(devaddr, (byte)Integer.parseInt(arg[3]));
				
				for(int i=0;i<buf.length;i++) {
					System.out.print(String.format("%02x ", buf[i]));
				}
				System.out.println("");
				
				return;
			}
			
			if(arg[1].equals("write"))
			{
				byte[] buf = new byte[arg.length - 3];
				
				for(int i=3;i<arg.length;i++) {
					buf[i-3] = (byte) Integer.parseInt(arg[i], 16);
				}
				sample.writeTo(devaddr, buf);
				return;
			}
			
			System.err.println("function must be [read] or [write]");
			printUsage();
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage()+": "+ lastError());
		}
	}
	
	/**
	 * Print usage for command line operations.
	 */
	protected static void printUsage() {
		System.out.println("I2C [device] read [0xaddress] [N]");
		System.out.println("I2C [device] write [0xaddress] [0xdata1 [0xdata2 ...]]");
	}
	
	/**
	 * Constructor. Open the device
	 */
	protected I2C(String dev) throws IOException {
		super();
		open(dev);
	}
	
	/**
	 * Destructor. Close the device
	 */
	protected void finalize() {
		close();
	}
	
	/**
	 * Ensure device is open
	 */
	private void checkOpen() throws IOException {
		if(m_fd == -1) {
			throw new IOException("Device is not open!");
		}
	}
	
	/**
	 * Open device.
     * @param dev Device filename
	 */
	private void open(String dev) throws IOException {
		close();
		m_fd = openDevice(dev);
	}
	
	/**
	 * Close device.
	 */
	private void close() {
		if(m_fd != -1) {
			close(m_fd);
			m_fd = -1;
		}
	}
	
	/**
	 * Get an instance of I2C device. Create new instance if needed.
	 * @param devname Device filename
	 * @return Handle to I2C bus.
	 */
	public static I2C getInstance(String devname) throws IOException {
		if(!m_buses.containsKey(devname)) {
			m_buses.put(devname, new I2C(devname));
		}
		return m_buses.get(devname);
	}
	
	/**
	 * Perform a read on the bus.
	 * @param address Address of chip on the bus
	 * @param length Number of bytes to request
	 * @return Byte array of readed bytes.
	 */
	public synchronized byte[] requestFrom(short address, short length) throws IOException {
		checkOpen();
		return requestFrom(m_fd, address, (short)0, length);
	}
	
	/**
	 * Write bytes on the bus.
	 * @param address Address of chip on the bus
	 * @param data Byte array to write on bus
	 */
	public synchronized void writeTo(short address, byte[] data) throws IOException {
		checkOpen();
		writeTo(m_fd, address, (short)0, data);
	}
	
	/**
	 * Write one byte on the bus.
	 * @param address Address of chip on the bus
	 * @param data Byte to write on bus
	 */
	public synchronized void writeTo(short address, byte data) throws IOException {
		byte[] dat = new byte[1];
		dat[0] = data;
		writeTo(address, dat);
	}
}


