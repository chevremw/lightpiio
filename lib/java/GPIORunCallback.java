/*
 * This file is part of LightPiIO.
 *
 * LightPiIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LightPiIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * 2019 - 2023 (c) William Chèvremont
 */
 
 
package org.lightpiio;

/**
 * This class handle a interruptible Thread running
 */
public abstract class GPIORunCallback implements Runnable {

	protected boolean m_continue=true; /**< Boolean holding if thread should stop or not. */
	
	/**
	 * Ask to stop thread execution.
	 * Thread may not stop immediately, but perform closing and memory freeing first.
	 */
	public void stop() {
		m_continue = false;
	}
}
