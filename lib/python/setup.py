
# This file is part of LightPiIO.
#
# LightPiIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LightPiIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont

from setuptools import setup, find_packages

setup(
    name='LightPiIO',
    version='@LightPiIO_VERSION@',
    install_requires=[
        'typeguard',
    ],
    packages=find_packages(include=['lightpiio', 'lightpiio.*']),
    package_data={'': ['*.so*']},
    include_package_data=True,
)


