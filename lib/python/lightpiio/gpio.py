
# This file is part of LightPiIO.
#
# LightPiIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LightPiIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont


import ctypes
import os
import re

from typeguard import typechecked
from enum import Enum
from .common import getLib

_lib = getLib()

class GPIOException(Exception):
    pass

class GPIO():
    
    __errorcodes = {
        0: "OK, no error!",
        -1: "Error.",
        -2: "IO Error",
        -3: "Pin is not exported",
        -4: "Unknown direction",
        -5: "Unknown trigger",
        -6: "File open error"
    }
    
    class Direction(Enum):
        INPUT = 1
        OUTPUT = 0
        
    class Trigger(Enum):
        NONE = 0
        RISING = 1
        FALLING = 2
        BOTH = 3
    
    @typechecked
    def __init__(self, gpioid : int):
        self.__gpioid = gpioid
        
    def __repr__(self):
        return f"<{self.__class__}({self.__gpioid})>"
    
    def __call_fnc(self, fnc, *args):
        r = fnc(self.__gpioid, *args)
        
        if r != 0:
            if r in self.__errorcodes:
                raise GPIOException(f"Error {r}: {self.__errorcodes[r]}")
            else:
                raise GPIOException(f"Unexpected error code {r}")
            
    @typechecked
    @classmethod
    def pinname_to_gpioid(cls, pinname : str):
        reg = re.compile(r'P([A-Z])([0-9]*)')
        m = reg.match(pinname)
            
        if m is None:
            return None
        
        grpid = m.group(1)
        off = int(m.group(2))
        
        return ((ord(grpid) - ord('A')) << 5) + off
            
    def export(self):
        self.__call_fnc(_lib.lightpiio_gpio_export)
        
    def unexport(self):
        self.__call_fnc(_lib.lightpiio_gpio_unexport)
        
    @property
    def isExported(self):
        exp = ctypes.c_bool()
        self.__call_fnc(_lib.lightpiio_gpio_isExported, ctypes.byref(exp))
        
        return bool(exp)
    
    @property
    def direction(self):
        dd = ctypes.c_ubyte()
        self.__call_fnc(_lib.lightpiio_gpio_getDirection, ctypes.byref(dd))
        
        return GPIO.Direction(dd.value)
    
    @typechecked
    def setDirection(self, v : 'GPIO.Direction'):
        self.__call_fnc(_lib.lightpiio_gpio_setDirection, ctypes.c_ubyte(v.value))
    
    @property
    def trigger(self):
        tt = ctypes.c_ubyte()
        self.__call_fnc(_lib.lightpiio_gpio_getTrigger, ctypes.byref(tt))
        
        return GPIO.Trigger(tt.value)
    
    @typechecked
    def setTrigger(self, t : 'GPIO.Trigger'):
        self.__call_fnc(_lib.lightpiio_gpio_setTrigger, ctypes.c_ubyte(t.value))
        
    @property
    def value(self):
        vv = ctypes.c_bool()
        self.__call_fnc(_lib.lightpiio_gpio_digitalRead, ctypes.byref(vv))
        
        return bool(vv)
    
    @typechecked
    def setValue(self, val : bool):
        self.__call_fnc(_lib.lightpiio_gpio_digitalWrite, ctypes.c_bool(val))
        
        
    @typechecked
    def waitForInterrupt(self, timeout : int = 30000):
        self.__call_fnc(_lib.lightpiio_gpio_waitForInterrupt, ctypes.c_uint64(timeout))


