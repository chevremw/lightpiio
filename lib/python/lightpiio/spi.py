
# This file is part of LightPiIO.
#
# LightPiIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LightPiIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with LightPiIO.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont

import ctypes
import os
import re

from typeguard import typechecked
from enum import Enum
from .common import getLib
from collections.abc import Sequence

_lib = getLib()

class SPIException(Exception):
    pass

class SPI():
    
    __errorcodes = {
        0: "OK, no error!",
        -1: "Error.",
        -2: "IO Error",
    }
    
    class Mode(Enum):
        MODE_0 = 0
        MODE_1 = 1
        MODE_2 = 2
        MODE_3 = 3
    
    @typechecked
    def __init__(self, device : str, mode : Mode, speed : int = 1000000):
        self.__device = device
        self.__mode = mode
        self.__speed = speed
        self.__fd = None
        
    def __del__(self):
        if self.__fd is not None:
            self.close()
        
    
    def __call_fnc(self, fnc, *args):
        if self.__fd is None:
            raise SPIException(f"Open device first.")
        
        r = fnc(self.__fd, *args)
        
        if r != 0:
            if r in self.__errorcodes:
                raise SPIException(f"Error {r}: {self.__errorcodes[r]}")
            else:
                raise SPIException(f"Unexpected error code {r}")
        
    def open(self):
        fd = ctypes.c_int()
        r = _lib.lightpiio_spi_open(ctypes.c_char_p(self.__device.encode()), ctypes.c_uint64(self.__speed), ctypes.c_uint64(self.__mode), ctypes.byref(fd))
        
        if r != 0:
            if r in self.__errorcodes:
                raise SPIException(f"Error {r}: {self.__errorcodes[r]}")
            else:
                raise SPIException(f"Unexpected error code {r}")
            
        self.__fd = fd
        
    def close(self):
        self.__call_fnc(_lib.lightpiio_spi_close)
        self.__fd = None
        
    def __enter__(self):
        self.open()
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        
    @typechecked
    def transfer(self, address : int, data : Sequence[int]):
        _add = ctypes.c_ushort(address)
        _len = ctypes.c_ushort(len(data))
        _buftype = ctypes.c_ubyte * len(data)
        _buftx = _buftype(*data)
        _bufrx = _buftype()
        
        self.__call_fnc(_lib.lightpiio_spi_transfer, _len, _txbuf, _rxbuf)
        
        return list(_rxbuf)
        
    



